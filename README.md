## Not Quite Aisles

Barebones grocery list sorter webapp using Python and Flask.

[Live demo](http://nqa.tylerwengerd.com)

All configured for deployment via the Heroku command line.

### Operation
Type in foods on your grocery list - use plurals or use [uncountable nouns](http://www.ef.com/english-resources/english-grammar/countable-and-uncountable-nouns/) (e.g. milk)

Clicking the submit button will bring up a list of your foods grouped by department for easier shopping (or an error if the food is unknown). You can go back without losing any information as well.

### Behind the scenes
There's a yaml file that has a list of foods and their associated departments.
A separate feature generates `static/departments.txt` for users to view if they so choose.

### To do:
* Add a lot more foods to the department list
* Add some automatic deployment
